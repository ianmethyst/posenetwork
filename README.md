# PoseNetwork - PoseNet sobre OSC

Este proyecto hace uso de [PoseNet](https://github.com/tensorflow/tfjs-models/tree/master/posenet) para analizar un feed de video de una webcam y enviar la información de la pose de una persona a través de la red utilizando OSC.

Como el paquete [osc](https://www.npmjs.com/package/osc) sólo puede enviar datos a través de WebSockets desde un navegador, el proyecto está construido sobre [Electron](https://electronjs.org/) para poder para enviar la información utilizando el protocolo UDP (gracias a la API de Node.js).

Se reimplementaron algunas clases del código original de PoseNet para poder usar modelos descargados de manera local, debido a qué el método PoseNet.load() utilizado para inicializar la biblioteca descarga los modelos de los servidores de Google cada vez que es llamado.  

De momento esto es solamente una **prueba de concepto**. No hay una interfaz para configurar ninguna de las variables en tiempo real, ni se puede compilar el proyecto para ser distribuido.

## Configuración de PoseNet

Como no hay una interfaz para configurar las variables, cualquier cambio se debe hacer desde el archivo `src/render/variables.js`. 

## Limitaciones

De momento, únicamente se puede utilizar el modo para estimar la pose de una sola persona (pese a que PoseNet puede estimar la pose de hasta 15 personas en simulteaneo).

Además, no se puede seleccionar la cámara a utilizar. De momento, una posible solución a esto es desconectar las webcams adicionales, o en el caso de no poder desconectarlas, utilizarlas en otro software antes de iniciar PoseNetwork.

Si se necesita usar el feed de la cámara en otra aplicación adicional, se puede utilizar [https://github.com/umlaeute/v4l2loopback](v4l2loopback) para crear webcams virtuales en Linux. 


## Dependencias

De momento, el proyecto sólo fue probado en Arch Linux con Node.js v10.15.3 

Teoricamente, PoseNetwork debería funcionar también en Windows y Mac OS X, y probablemente también con versiones previas o posteriores de Node.js. 

En el caso de que se encuentre algún tipo de error, un buen punto de partida para intentar solucionarlo sería instalar la versión de Node.js utilizada en el proyecto. Para cambiar rápidamente entre versiones de Node.js, se recomienda utilizar [nvm](https://github.com/nvm-sh/nvm).

## Configuración inicial

En primer lugar se debe clonar el repositorio:

```sh
git clone https://gitlab.com/ianmethyst/PoseNetwork
```

Con el comando previo, se creará un nuevo directorio bajo el directorio actual con el nombre de PoseNetwork. Cambiar el directorio actual a éste con:

```sh
cd PoseNetwork
```

A continuación, se puede cambiar a la versión de Node.js (con [nvm](https://github.com/nvm-sh/nvm)) a la que se utilizó para dearrollar PoseNetwork (opcional):

```sh
$ nvm use
```

A continuación se deben instalar las dependencias de npm:

```sh
npm install 
```

Una de las [dependencias](https://serialport.io/docs/guide-installation#electron) debe ser recompilada con:

```sh
npm run rebuild
```

Finalmente, se deben descargar los modelos de PoseNet de los servidores de Google para poder usarlos de manera local. En el caso de que el siguiente comando falle, debe ser reingresado hasta que se complete correctamente: 

```sh
npm run getModels
```

# Uso

Como de momento no se puede compilar el proyecto, la única forma de utilizarlo es iniciando el servidor de desarrollo (una vez terminada la [configuración inicial](#configuracion-inicial)) con el siguiente comando:

```sh
npm run dev
```

## Ejemplo

Una vez iniciado el servidor de desarrollo, en el caso de que se haya inicializado la webcam correctamente (debe haberse abierto una ventana que muestre su imagen), en cualquier momento que se detecte una persona, la información de cada [punto clave](#puntos-clave) será enviada a la dirección `127.0.0.1:12000`.

En el directorio `processing` se encuentra un sketch de [Processing](https://processing.org/) que permite mover un circulo en la ventana al mover la muñeca izquierda (punto clave `/leftWrist`). El ejemplo depende en la librería [oscP5](http://www.sojamo.de/libraries/oscP5/).

![Captura de pantalla de ejemplo en Processing](./screenshot.png)

## Puntos clave

En el caso de que se detecte una pose, las coordenadas (`x`, `y`. Ambas como `float32`) son enviadas a los siguientes patrones de direccion: 

| Id | Punto clave |
| -- | -- |
| 0 | /nose |
| 1 | /leftEye |
| 2 | /rightEye |
| 3 | /leftEar |
| 4 | /rightEar |
| 5 | /leftShoulder |
| 6 | /rightShoulder |
| 7 | /leftElbow |
| 8 | /rightElbow |
| 9 | /leftWrist |
| 10 | /rightWrist |
| 11 | /leftHip |
| 12 | /rightHip |
| 13 | /leftKnee |
| 14 | /rightKnee |
| 15 | /leftAnkle |
| 16 | /rightAnkle |
