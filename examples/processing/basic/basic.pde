import oscP5.*;

OscP5 oscP5;

PVector p; // Posición de elipse

void setup() {
  size(640,480);
  oscP5 = new OscP5(this,12000);
  
  p = new PVector(width / 2, height / 2);
    
  /*
   * Con OscP5.plug(), Al recibir un mensaje en la dirección /leftWrist, los datos
   * se convierten en argumentos para la función "setPos". Ver posibles direcciones en:
   * https://www.npmjs.com/package/@tensorflow-models/posenet#keypoints
   */
   
  oscP5.plug(this, "setPos", "/leftWrist");
}

public void setPos(float x, float y) {
  p.set(x, y);
}

void draw() {
  background(0);
  
  fill(255);
  ellipse(p.x, p.y, 50, 50);
}
