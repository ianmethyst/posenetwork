import oscP5.*;

OscP5 oscP5;

int size = 30;

PVector r, l; // Posiciones de elipses

void setup() {
  size(800, 600, P3D);
  noStroke();

  oscP5 = new OscP5(this, 12000);

  r = new PVector(width - size * 2, height / 2);
  l = new PVector(size * 2, height / 2);

  /*
   * Con OscP5.plug(), Al recibir un mensaje en la dirección /leftWrist y /righWrist, los 
   * datos se convierten en argumentos para la funciones "left" y "right" respectivamente.
   * Ver posibles direcciones en:
   * https://www.npmjs.com/package/@tensorflow-models/posenet#keypoints
   */

  oscP5.plug(this, "left", "/leftWrist");
  oscP5.plug(this, "right", "/rightWrist");
}

/*
 * Los valores recibidos conrresponden a los valores de las dimensiones del feed de la
 * camara, por lo que en el caso de que la ventana tenga otras dimensiones, esas 
 * coordenadas tienen que ajustadas proporcionalmente.
 */

public PVector mapPVector(float x, float y) {
  float mappedX = map(x, 0, 640, 0, width);  
  float mappedY = map(y, 0, 480, 0, height);

  return new PVector(mappedX, mappedY);
}

/*
 * De vez en cuando, PoseNet confunde la muñeca izquierda con la derecha. Esta función
 * se encarga de comprobar cuando pasa eso para determinar si debe o no asignar la nueva
 * posición recibida
 */

public boolean checkDist(PVector actual, PVector data) {
  if (PVector.dist(actual, data) > 80) {
    return true;
  } else {
    return false;
  }
}

public void left(float x, float y) {
  PVector data = mapPVector(x, y);
  if (checkDist(r, data)) {
    l.set(data);
  }
}

public void right(float x, float y) {
  PVector data = mapPVector(x, y);
  if (checkDist(l, data)) {
    r.set(data);
  }
}

void draw() {
  background(0);
  lights();

  // Box
  float zoom = PVector.dist(l, r) * 2;
  ortho(-width/2 - zoom, width/2 + zoom, -height/2 - zoom, height/2 + zoom);

  pushMatrix();
  translate(width/2, height/2, 0);

  rotateX(-PI/6); 
  rotateY((atan2(l.y - r.y, l.x - r.x))); 

  fill(255);
  box(255); 
  popMatrix();

  // Elipses
  ortho(-width/2, width/2, -height/2, height/2);

  pushMatrix();
  fill(255, 0, 0);
  ellipse(r.x, r.y, 50, 50);

  fill(0, 255, 0);
  ellipse(l.x, l.y, 50, 50);
  popMatrix();
}
