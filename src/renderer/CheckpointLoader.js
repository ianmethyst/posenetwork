import fs from 'fs';
import path from 'path';
import { promisify } from 'util';

import { tensor } from '@tensorflow/tfjs';

const readFileAsync = promisify(fs.readFile);
const MANIFEST_FILE = 'manifest.json';

class CheckpointLoader {
  constructor(dir) {
    this.dir = dir;
  }

  async loadManifest() {
    try {
      const m = await readFileAsync(path.join(__dirname, this.dir, 'manifest.json'));
      this.manifest = JSON.parse(m);
      return;
    } catch (error) {
      throw new Error(`${MANIFEST_FILE} not found at ${this.dir}: ${error}`)
    }
  }

  async getCheckpointManifest() {
    if (this.manifest == null) {
      await this.loadManifest();
    }
    return this.manifest;
  }

  async getAllVariables() {
    if (this.variables != null) {
      return this.variables;
    }

    if (this.manifest == null) {
      await this.loadManifest();
    }

    const variableNames = Object.keys(this.manifest);
    const variablePromises = [];

    for (let i = 0; i < variableNames.length; i++) {
      variablePromises.push(this.getVariable(variableNames[i]));
    }

    await Promise.all(variablePromises).then(variables => {
      this.variables = {};
      for (let i = 0; i < variables.length; i++) {
        this.variables[variableNames[i]] = variables[i];
      }
    });

    return this.variables;
  }

  async getVariable(varName) {
    if (!(varName in this.manifest)) {
      throw new Error('Cannot load non-existant variable ' + varName);
    }

    if (this.manifest == null) {
      await this.loadManifest();
    }

    const variableRequest = async () => {
      const fname = this.manifest[varName].filename;

      try {
        const file = await readFileAsync(path.join(__dirname, this.dir, fname));
        const values = new Float32Array(toArrayBuffer(file));

        const checkpointTensor =
          tensor(values, this.manifest[varName].shape, 'float32');

        return checkpointTensor;
      } catch (error) {
        throw new Error(`Could not read variable ${varName}: ${error}`);
      }
    }

    return variableRequest();
  }
}

export default CheckpointLoader;

function toArrayBuffer(myBuf) {
   var myBuffer = new ArrayBuffer(myBuf.length);
   var res = new Uint8Array(myBuffer);
   for (var i = 0; i < myBuf.length; ++i) {
      res[i] = myBuf[i];
   }
   return myBuffer;
}
