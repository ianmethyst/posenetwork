const variables = {
  POSENET: {
    MULTIPLIER: 0.75,
    MIN_POSE_CONFIDENCE: 0.4,
    MIN_PART_CONFIDENCE: 0.8,
    IMAGE_SCALE_FACTOR: 0.5,
    FLIP_HORIZONTAL: true,
    OUTPUT_STRIDE: 8
  },
  VIDEO: {
    WIDTH:  640,
    HEIGHT:  480,
  },
  OSC: {
    LOCAL_ADDRESS: '0.0.0.0',
    LOCAL_PORT: 57121,
    REMOTE_ADDRESS: '127.0.0.1',
    REMOTE_PORT: 12000
  }
}

export default variables;

