import * as posenet from '@tensorflow-models/posenet';
import osc from 'osc';

import { loadModel } from './models';

import { 
  drawSkeleton,
  drawKeypoints, 
  drawVideo
} from './util';

import v from './variables';

const constraints = {
  audio: false,
  video: { 
    facingMode: "user",
    width: v.VIDEO.WIDTH,
    height: v.VIDEO.HEIGHT
  }
}

// Create an osc.js UDP Port listening on port 57121.
const udpPort = new osc.UDPPort({
    localAddress: v.OSC.LOCAL_ADDRESS,
    localPort: v.OSC.LOCAL_PORT,
    metadata: true
});

// Open the socket.
udpPort.open();

const render = async () => {
  const body = document.querySelector('body').setAttribute('style', 'margin: 0; padding: 0');
  
  const videoElement = document.createElement("video");
  videoElement.setAttribute('id', 'video');
  videoElement.setAttribute('autoplay', 'true');
  videoElement.setAttribute('style', 'display: none');

  navigator.getUserMedia = navigator.webkitGetUserMedia;
  navigator.getUserMedia(
    constraints,
    stream => videoElement.srcObject = stream,
    error => console.error(error));

  const canvasElement = document.createElement("canvas");
  canvasElement.setAttribute('id', 'output');
  const ctx = canvasElement.getContext('2d');

  const app = document.querySelector('#app');
  app.setAttribute('style', 'display: flex; align-items: center; justify-content: center');
  app.appendChild(videoElement);
  app.appendChild(canvasElement);

  videoElement.width = v.VIDEO.WIDTH;
  videoElement.height = v.VIDEO.HEIGHT;
  canvasElement.width = v.VIDEO.WIDTH;
  canvasElement.height = v.VIDEO.HEIGHT;

  const net = await loadModel(v.POSENET.MULTIPLIER);

  async function loop() {
    const pose = await net.estimateSinglePose(
      videoElement,
      v.POSENET.IMAGE_SCALE_FACTOR,
      v.POSENET.FLIP_HORIZONTAL,
      v.POSENET.OUTPUT_STRIDE
    );

    drawVideo(ctx, videoElement, v.VIDEO.WIDTH, v.VIDEO.HEIGHT);

    drawSkeleton(pose.keypoints, v.POSENET.MIN_POSE_CONFIDENCE, ctx);
    drawKeypoints(pose.keypoints, v.POSENET.MIN_PART_CONFIDENCE, ctx);

    pose.keypoints.forEach(k => {
      if (k.score >= v.POSENET.MIN_PART_CONFIDENCE) {

        udpPort.send({
          address: `/${k.part}`, args: [
            { type: "f", value: k.position.x },
            { type: "f", value: k.position.y }
          ]}, v.OSC.REMOTE_ADDRESS, v.OSC.REMOTE_PORT);
      }
    });

    await requestAnimationFrame(loop);
  };

  loop();
}

render();
