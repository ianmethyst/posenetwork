import * as posenet from '@tensorflow-models/posenet';
import fs from 'fs';
import path from 'path';

import CheckpointLoader from './CheckpointLoader';
import ModelWeights from './ModelWeights';

import checkpoints from './checkpoints';

async function loadMobileNetModel (multiplier) {
  const checkpoint = checkpoints[multiplier];

  const checkpointLoader = new CheckpointLoader(checkpoint.url);
  const variables = await checkpointLoader.getAllVariables();

  const modelWeights = new ModelWeights(variables);

  return new posenet.MobileNet(modelWeights, checkpoint.architecture);
}

export async function loadModel(multiplier) {
  const mobileNetModel = await loadMobileNetModel(multiplier);

  return new posenet.PoseNet(mobileNetModel);
}
