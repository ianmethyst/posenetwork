import * as posenet from '@tensorflow-models/posenet';

const BASE_URL = '/models/';

const checkpoints = {
  1.01: {
    url: BASE_URL + 'mobilenet_v1_101/',
    architecture: posenet.mobileNetArchitectures[100]
  },
  1.0: {
    url: BASE_URL + 'mobilenet_v1_100/',
    architecture: posenet.mobileNetArchitectures[100]
  },
  0.75: {
    url: BASE_URL + 'mobilenet_v1_075/',
    architecture: posenet.mobileNetArchitectures[75]
  },
  0.5: {
    url: BASE_URL + 'mobilenet_v1_050/',
    architecture: posenet.mobileNetArchitectures[50]
  }
};

export default checkpoints;
